# Changelog
All notable changes to `techendeavors/fileinfo` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2018-07-08
### Added
- Boolean checks
- File event dates
- Basic Hashing
- Path info and mime types
- File Permission info
- File size calculator

## [1.1.0] - 2018-07-12
### Added
- Ability to check to see if Carbon is installed and output dates in Carbon format
- Added a hashing benchmark and friendly microtime output method. Will be taken out and put into another package soon
- Now lets you specify what hashes process using the config file
- Allows you to set a maximum file size for hashing
- Now shows how much space a directory takes

### Changed
- Changed function name from `examine` to `all`
- Now uses variable set in $this so it doesn't have to be passed to every function
- Object is now mostly arrays rather than sub objects

### Removed
- 

## [1.2.0] - 2018-07-12
### Added

### Changed
- Refactored to stop using the facade
- Can now call statically
- Made most of the traits private and static. Will move some to public again soon

### Removed
- 
