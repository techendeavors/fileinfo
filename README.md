# FileInfo

## Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/techendeavors/fileinfo.svg?style=flat-square)](https://packagist.org/packages/techendeavors/fileinfo)
[![Total Downloads](https://img.shields.io/packagist/dt/techendeavors/fileinfo.svg?style=flat-square)](https://packagist.org/packages/techendeavors/fileinfo)

Feed it a file location and get back information like its mimetype, hashes, size, creation and modification date, symlink chains, permissions and ownership, etc.

### To Do

- [X] If file is beyond a certain size, do not hash it automatically when requesting all the data
- [X] Allow for specific hash tests
- [ ] Make everything a lot more fluent
- [ ] Return a proper collection
- [ ] Allow for hash grouping tests and set one to be the default
- [X] check for hash algorithm before running
- [X] Allow file name to be specified during the creation
- [ ] Allow static access
- [ ] Add Techendeavors\FriendlyScale results to FileSize output if the package is loaded
- [ ] Transition some integrated scaling functions to Techendeavors\FriendlyScale
- [X] Make Permissions a collection that has octal and human readable permission information 
- [ ] Look into listing ACL info
- [ ] Output current user and group the check is running under
- [X] Move user and group info to an Ownership collection. 
- [X] Make a getEvents class that includes the file creation and modification date
- [X] Convert the functions to traits
- [ ] Optional modules to check file hashes against various databases (perhaps another package?)
  - [ ] https://totalhash.com/
  - [ ] https://www.team-cymru.com/mhr.html#dns (DNS using Techendeavors\DNSOverHttps)
  - [ ] https://www.owasp.org/index.php/OWASP_File_Hash_Repository (DNS using Techendeavors\DNSOverHttps)
  - [ ] http://nsrl.hashsets.com/national_software_reference_library1_search.php
  - [ ] https://github.com/jessek/hashdeep
  - [ ] https://www.forensicswiki.org/wiki/Hashing
  - [ ] https://www.hashsets.com/hash-set-databases/
  - [ ] https://VirusTotal.com
  - [ ] https://www.nist.gov/forensics/forensic-database-tech-digital-evidence-table
- [ ] Write some exceptions
- [ ] Write Tests

### Development Process

Before the package is registered with [packagist.org](https://packagist.org), you can work with it locally. First, add the following to your `.bashrc`, `.bash_profile`, or `.zshrc`

```bash
composer-link() {  
    composer config repositories.local '{"type": "path", "url": "'$1'"}' --file composer.json
}
```

Then, in a Laravel installation folder, run `composer-link ../fileinfo` and it'll add the package information to the repositories section of the composer.json file. Then run `composer require techendeavors/fileinfo`

You can [find more information here](http://calebporzio.com/bash-alias-composer-link-use-local-folders-as-composer-dependancies/).

### Installation

You can install the package via composer:

```bash
composer require techendeavors/fileinfo
```

### Usage

Useful file output

``` 
>>> $test = new FileInfo('/usr/bin/ffmpeg')
=> Techendeavors\FileInfo\FileInfo {#2855}
>>> $test->all()
=> Illuminate\Support\Collection {#2849
     all: [
       "query" => "/usr/bin/ffmpeg",
       "tests" => [
         "directory" => false,
         "executable" => true,
         "file" => true,
         "link" => false,
       ],
       "permissions" => [
         "readable" => true,
         "writable" => false,
         "user" => "root",
         "group" => "root",
         "octal" => "0755",
         "human" => "rwxr-xr-x",
       ],
       "info" => [
         "name" => "ffmpeg",
         "path" => "/usr/bin",
         "type" => "file",
         "mime" => "application/x-sharedlib",
       ],
       "size" => [
         "bytes" => 272528,
         "human" => "266.14KB",
       ],
       "hashes" => [
         "crc32" => "ec9b4c8f",
         "crc32b" => "b5be398b",
         "md5" => "7f7f8d7d5549ec1deabaff3f615f80ce",
         "ripemd256" => "8fc001f0f9395f14ceb652cdef4d4a54caa33dae549a002e95ad45c8365ff4cc",
         "sha1" => "f43e9bb580a156aa3f84ba15d6965ac23d86b5d0",
         "sha256" => "290bb7c6a8bfdd308b2a59c3fd8f3655aec224646a9f348920a81d492db85aef",
         "sha3-256" => "7ef630dda0bc7484f00cd7e9c4268ee3c886212121991c0e94ab49e0c799cdfb",
         "sha3-384" => "cfb8fba5ee465a81c6c0d9ba2171d248fa902ec0a288cfb284f319aa7bbe6e2310e2f33252596d78de5ef8e3af3c78ba",
         "sha3-512" => "3fceb5e875610d283d223e10f914af02a7d367247dd00e87dff33e61afa7364e814a84ce2b831d91042605051e1d2371a3004ba0ca5dbd804dd8cf6cf843eeeb",
         "sha384" => "8e8a8dbb4542f55152cf77ed44d304febdc3182a775e54238c5d85a942d30b226347475a148b36f2e7638fedd5b0dab6",
         "sha512" => "0f3a69a3dada9ac0f12111a9c6c901d29c17fc30510764278a3cb52c1c0b61f57ad33e9197dda5f673452105df9d117b3fb99c7feeb330ffc55dae893325f859",
       ],
       "events" => [
         "modified" => Carbon\Carbon @1523273080 {#2866
           date: 2018-04-09 11:24:40.0 UTC (+00:00),
         },
         "changed" => Carbon\Carbon @1526714923 {#2861
           date: 2018-05-19 07:28:43.0 UTC (+00:00),
         },
         "accessed" => Carbon\Carbon @1531345222 {#2856
           date: 2018-07-11 21:40:22.0 UTC (+00:00),
         },
       ],
     ],
   }
```

Even works with directories
```
>>> $test = new FileInfo('/usr/bin')
=> Techendeavors\FileInfo\FileInfo {#2864}
>>> $test->all()
=> Illuminate\Support\Collection {#2861
     all: [
       "query" => "/usr/bin",
       "tests" => [
         "directory" => true,
         "executable" => true,
         "file" => false,
         "link" => false,
       ],
       "permissions" => [
         "readable" => true,
         "writable" => false,
         "user" => "root",
         "group" => "root",
         "octal" => "755",
         "human" => "rwxr-xr-x",
       ],
       "info" => [
         "name" => "bin",
         "path" => "/usr",
         "type" => "dir",
         "mime" => "directory",
       ],
       "size" => [
         "bytes" => 980864346,
         "human" => "935.43MB",
       ],
       "hashes" => null,
       "events" => [
         "modified" => Carbon\Carbon @1531375218 {#2854
           date: 2018-07-12 06:00:18.0 UTC (+00:00),
         },
         "changed" => Carbon\Carbon @1531375218 {#2855
           date: 2018-07-12 06:00:18.0 UTC (+00:00),
         },
         "accessed" => Carbon\Carbon @1531375223 {#2849
           date: 2018-07-12 06:00:23.0 UTC (+00:00),
         },
       ],
     ],
   }
>>> 

```

### Testing

Not yet implemented

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please use the [issue tracker](https://gitlab.com/techendeavors/fileinfo)

### Credits

- [Techendeavors](https://gitlab.com/techendeavors)
- [Contributors and Credits](CONTRIBUTORS.md)



### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
