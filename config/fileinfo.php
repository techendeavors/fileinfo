<?php

return [

/**
 * FileInfo Settings
 */

    /**
     * When you view the default file information, it calculates multiple hash values.
     * This can be a bad thing if the file is too big. The default limit is 20 megabytes
     */
    'hash_max_size' => 1024 * 1024 * 20,

    /** 
     * What hash algorithms to use by default. For a full list, run \Techendeavors\FileInfo::hashAlgorithms();
     * If an algorithm listed here isn't actually available, it'll be skipped.
     */
    'hash_algos' => [
        'crc32',
        'crc32b',
        'md5',
        'ripemd256',
        'sha1',
        'sha256',
        'sha3-256',
        'sha3-384',
        'sha3-512',
        'sha384',
        'sha512',
    ],
        
];
