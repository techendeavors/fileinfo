<?php

namespace Techendeavors\FileInfo;

use Illuminate\Support\Collection;
use Techendeavors\FileInfo\Traits\Checks;
use Techendeavors\FileInfo\Traits\Events;
use Techendeavors\FileInfo\Traits\Hashes;
use Techendeavors\FileInfo\Traits\Info;
use Techendeavors\FileInfo\Traits\Permissions;
use Techendeavors\FileInfo\Traits\Size;
use Techendeavors\FileInfo\Traits\TestResults;

class FileInfo
{
    use Checks,
        Events,
        Hashes,
        Info,
        Permissions,
        Size,
        TestResults;

    protected $filename;

    const DEFAULT_HASH_MAX_LENGTH = (1024 * 1024 * 20);

    const DEFAULT_HASH_ALGOS = ['crc32','crc32b','md5','ripemd256','sha1','sha256','sha3-256','sha3-384','sha3-512','sha384','sha512'];

    /**
     * Create a new instance.
     */
    public function __construct(string $filename = null)
    {
        $this->filename = $filename;
    }

    /**
     * Run all at once
     *
     * @param string $phrase Phrase to return
     * @return string Returns the phrase passed in
     */
    public static function show($filename)
    {
        return collect([
            "query" => $filename,
            "tests" => static::getTestResults($filename),
            "permissions" => static::getPermissions($filename),
            "info" => static::getInfo($filename),
            "size" => static::getSize($filename),
            "hashes" => static::getHashes($filename),
            "events" => static::getEvents($filename)
        ]);
    }
}
