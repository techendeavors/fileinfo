<?php

namespace Techendeavors\FileInfo;

use Illuminate\Support\ServiceProvider;

class FileInfoServiceProvider extends ServiceProvider
{
    /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/fileinfo.php' => config_path('fileinfo.php'),
        ], 'config');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/fileinfo.php', 'fileinfo'
        );
    }

}
