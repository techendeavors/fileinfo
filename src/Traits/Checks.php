<?php

namespace Techendeavors\FileInfo\Traits;

trait Checks
{
    protected static function isThere($filename)
    {
        return (bool) (file_exists($filename));
    }

    protected static function isNotThere($filename)
    {
        return (bool) (! static::isThere($filename));
    }

    protected static function isReadable($filename)
    {
        return (bool) (is_readable($filename));
    }

    protected static function isNotReadable($filename)
    {
        return (bool) (! static::isReadable($filename));
    }

    protected static function isWritable($filename)
    {
        return (bool) (is_writable($filename));
    }

    protected static function isNotWritable($filename)
    {
        return (bool) (! static::isWritable($filename));
    }

    protected static function isDirectory($filename)
    {
        return (bool) (is_dir($filename));
    }

    protected static function isNotDirectory($filename)
    {
        return (bool) (! static::isDirectory($filename));
    }

    protected static function isExecutable($filename)
    {
        return (bool) (is_executable($filename));
    }

    protected static function isNotExecutable($filename)
    {
        return (bool) (! static::isExecutable($filename));
    }

    protected static function isFile($filename)
    {
        return (bool) (is_file($filename));
    }

    protected static function isNotFile($filename)
    {
        return (bool) (! static::isFile($filename));
    }

    protected static function isLink($filename)
    {
        return (bool) (is_link($filename));
    }

    protected static function isNotLink($filename)
    {
        return (bool) (! static::isLink($filename));
    }

}
