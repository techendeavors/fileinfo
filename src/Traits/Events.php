<?php

namespace Techendeavors\FileInfo\Traits;

trait Events
{
    protected static function getModifiedTime($filename)
    {
        return (object) static::dateTimeObject(filemtime($filename));
    }

    protected static function getChangedTime($filename)
    {
        return (object) static::dateTimeObject(filectime($filename));
    }

    protected static function getAccessedTime($filename)
    {
        return (object) static::dateTimeObject(fileatime($filename));
    }

    protected static function getSystemTimeZone()
    {
        return (object) (new \DateTimeZone(trim(shell_exec("date +%:z"))));
    }

    protected static function dateTimeObject(int $timestamp)
    {
        $timezone = self::GetSystemTimeZone();

        if (class_exists('\Carbon\Carbon')) {
            return (object) (\Carbon\Carbon::createFromTimestamp($timestamp, $timezone)->setTimeZone(date_default_timezone_get()));
        } else {
            return (object) (\DateTime::createFromFormat("U", $timestamp, $timezone)->setTimeZone(date_default_timezone_get()));
        }
    }

    protected static function getEvents($filename)
    {
        if (self::isThere($filename)) {
            return array(
                "modified" => static::getModifiedTime($filename),
                "changed" => static::getChangedTime($filename),
                "accessed" => static::getAccessedTime($filename),
            );
        }
    }
}
