<?php

namespace Techendeavors\FileInfo\Traits;

trait Hashes
{
    private static function microtimeHuman($time)
    {
        if ($time == 0) {
            return 0;
        }
        $unit=array(
            -4 => 'ps',
            -3 => 'ns',
            -2 => 'mcs',
            -1 => 'ms',
             0 => 's');
        $picker=min(0, floor(log($time, 1000)));

        $computed = @round($time/pow(1000, $picker), 1);
        return $computed." ".$unit[$picker];
    }

    public static function hashAvailable(string $algorithm)
    {
        return (bool) (in_array(strtolower($algorithm), hash_algos()));
    }

    public static function hashBenchmark($format = false, $sort = "time")
    {
        $benchmark = random_bytes(16);

        $algos = collect();

        foreach (hash_algos() as $algo) {
            $time_start = microtime(true);
            for( $i = 0; $i<100; $i++ ) {
                $hash = hash($algo, $benchmark);
            }
            $time_end = microtime(true);
            $time = $time_end - $time_start;
            $length = strlen($hash);

            $algos->push([
                'algo' => $algo,
                'time' => $time,
                'human_time' => self::microtimeHuman($time),
                'length' => $length]);
        }

        $sorted = $algos->sortBy($sort);

        if ($format) {
            $output = sprintf("%-16s  %-22s  %-12s  %-8s", "Algorithm", "Exact Time", "Easy Time", "Length") . PHP_EOL;
            $output .= sprintf("%-16s+-%-22s+-%-12s+-%-8s", "----------------", "----------------------", "------------", "--------") . PHP_EOL;
            foreach ($sorted as $line) {
                $output .= vsprintf("%-16s| %-22s| %-12s| %-8s", $line) . PHP_EOL;
            }

            return (string) $output;
        }
        return (object) $algos;
    }

    private static function hashEngine($filename, string $algorithm)
    {
        if (static::hashAvailable($algorithm)) {
            return (string) hash_file($algorithm, $filename);
        }
    }

    private static function getHashes($filename, $hash_size = self::DEFAULT_HASH_MAX_LENGTH, $hash_algos = self::DEFAULT_HASH_ALGOS)
    {
        //$hash_max_size = config('fileinfo.hash_max_size') ?? (1024 * 1024 * 20);

        //$hash_algos = config('fileinfo.hash_algos') ?? ['crc32','crc32b','md5','ripemd256','sha1','sha256','sha3-256','sha3-384','sha3-512','sha384','sha512'];

        if ((static::isFile($filename)) && (static::isReadable($filename)) && (static::getFileBytes($filename) <= $hash_size)) {

            $hashes = collect();

            foreach ($hash_algos as $algo) {
                $hashes->put($algo, static::hashEngine($filename, $algo));
            }

            return (array) $hashes->toArray();
        }
    }
}
