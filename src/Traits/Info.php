<?php

namespace Techendeavors\FileInfo\Traits;

trait Info
{

    private static function getRealPath($filename)
    {
        return (string) (realpath($filename));
    }

    private static function getFileName($filename)
    {
        return (string) (basename($filename));
    }

    private static function getFilePath($filename)
    {
        return (string) (dirname($filename));
    }

    private static function getType($filename)
    {
        return (string) (filetype($filename));
    }

    private static function getMime($filename)
    {
        return (string) (mime_content_type($filename));
    }

    private static function getInfo($filename)
    {
        if (static::isThere($filename)) {
            return array(
                "name" => static::getFileName($filename),
                "path" => static::getFilePath($filename),
                "type" => static::getType($filename),
                "mime" => static::getMime($filename),
            );
        }
    }
}
