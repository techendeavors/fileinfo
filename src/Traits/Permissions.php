<?php

namespace Techendeavors\FileInfo\Traits;

trait Permissions
{
    private static function getOctalPermissions($filename)
    {
        return (string) substr(decoct(fileperms($filename)), 2);
    }

    private static function getHumanPermissions($filename)
    {
        $perms = fileperms($filename);

        $info = "";
        
        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ? (($perms & 0x0800) ? 's' : 'x' ) : (($perms & 0x0800) ? 'S' : '-'));

        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ? (($perms & 0x0400) ? 's' : 'x' ) : (($perms & 0x0400) ? 'S' : '-'));

        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ? (($perms & 0x0200) ? 't' : 'x' ) : (($perms & 0x0200) ? 'T' : '-'));

        return (string) $info;
    }

    private static function getUserName($filename)
    {
        return (string) (posix_getpwuid(fileowner($filename))["name"]);
    }

    private static function getGroupName($filename)
    {
        return (string) (posix_getgrgid(filegroup($filename))["name"]);
    }

    private static function getPermissions($filename)
    {
        if (self::isThere($filename)) {
            return array(
                "readable" => static::isReadable($filename),
                "writable" => static::isWritable($filename),
                "user" => static::getUserName($filename),
                "group" => static::getGroupName($filename),
                "octal" => static::getOctalPermissions($filename),
                "human" => static::getHumanPermissions($filename),
            );
        }
    }

}
