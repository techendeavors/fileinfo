<?php

namespace Techendeavors\FileInfo\Traits;

trait Size
{
    private static function getFileBytes($filename)
    {
        return (int) filesize($filename);
    }

    private static function getDirectoryBytes($filename)
    {
        return (int) array_filter(explode("\t", trim(shell_exec('du -sb0 ' . escapeshellcmd($filename) . ' 2>/dev/null'))))[0];
    }

    private static function getHumanSize(int $bytes, int $decimals = 2)
    {
        $factor = (int) floor((strlen($bytes) - 1) / 3);
        if ($factor > 0) {
            $unit = 'KMGT';
        } else {
            $unit = null;
        }
        return (string) sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$unit[$factor - 1] . 'B';
    }

    private static function getSize($filename)
    {
        if (static::isThere($filename)) {
            if (static::isFile($filename)) {
                $bytes = static::getFileBytes($filename);
            }

            if (static::isDirectory($filename)) {
                $bytes = static::getDirectoryBytes($filename);
            }

            return array(
                "bytes" => $bytes,
                "human" => static::getHumanSize($bytes),
            );
        }

    }
}
