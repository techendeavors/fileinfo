<?php

namespace Techendeavors\FileInfo\Traits;

trait TestResults
{
    private static function getTestResults($filename)
    {
        if (static::isThere($filename)) {
            return array(
                "directory" => static::isDirectory($filename),
                "executable" => static::isExecutable($filename),
                "file" => static::isFile($filename),
                "link" => static::isLink($filename),
            );
        }
    }
}
